#!/usr/bin/python3

# Student name and No.: PO, Wan Fung 3035283297
# Student name and No.: YIP, Po Lung 3035289368
# Development platform: macOS 10.14.4
# Python version: 3.7.3
# Version: 1.0-SNAPSHOT


import socket
import time
import threading
from tkinter import *

#
# Global variables
#
global user, serv_addr, serv_port, my_port, state, room

# client to connect room server
sockfd = socket.socket()
userlist = {}
# userlist with message ID for comparison
mlist = {}
backward_link = set()
forward_link = ()
msgID = 0
member_hash = 0
forward_socket = socket.socket()
backward_socket = {}
tcp_server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
udp_server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
running_thread = []
only_join = False
lock = threading.Lock()
#
# This is the hash function for generating a unique
# Hash ID for each peer.
# Source: http://www.cse.yorku.ca/~oz/hash.html
#
# Concatenate the peer's username, str(IP address), 
# and str(Port) to form a string that be the input 
# to this hash function
#
def sdbm_hash(instr):
    hash = 0
    for c in instr:
        hash = int(ord(c)) + (hash << 6) + (hash << 16) - hash
    return hash & 0xffffffffffffffff


class User:
    def __init__(self, name, IP, port):
        self.name = name
        self.IP = IP
        self.port = port


class Keepalive_class(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.event = threading.Event()

    def run(self):
        while not self.event.is_set():
            self.event.wait(20)
            join_request()



    def exit(self):
        self.event.set()

keepalive_class = Keepalive_class()

# UDP server is used to receive POKE (UDP message)
def UDPserver():
    global udp_server_socket

    udp_server_socket.bind(("", my_port))

    while udp_server_socket:
        # receive poke message
        try:
            data, addr = udp_server_socket.recvfrom(1024)
            rmsg = data.decode('ascii')
            _rmsg = rmsg.split(":")
            if _rmsg[0] == "K":
                MsgWin.insert(1.0, "\n~~~~[" + _rmsg[2] + "]Poke~~~~")
                # send back ACK
                udp_server_socket.sendto(b"A::\r\n", addr)
        except:
            udp_server_socket.close()
            break
#
# Functions to handle user input
#


def do_User():
    global user, state
    # handle rename issue
    if state == "JOINED" or state == "CONNECTED":
        CmdWin.insert(1.0, "\nCan only rename before joining a chatroom")
        return
    # handle empty string issue
    user = userentry.get()
    if not user:
        outstr = "\nUsername cannot be empty"
    else:
        outstr = "\n[User] username: " + user
        state = "NAMED"
    CmdWin.insert(1.0, outstr)
    userentry.delete(0, END)


def do_List():
    request = "L::\r\n"
    try:
        sockfd.send(request.encode('ascii'))
        rmsg = sockfd.recv(1024).decode('ascii').split(":")
        # handle error response
        if rmsg[0] == "F":
            CmdWin.insert(1.0, "\nError: " + rmsg[1])
            return
        # handle no chatrooms
        if rmsg[1] == '':
            CmdWin.insert(1.0, "\nThere are no active chatrooms")
            return
        # list the active chatrooms
        for i in range(1, len(rmsg) - 2):
            CmdWin.insert(1.0, "\n\t" + rmsg[i])
        CmdWin.insert(1.0, "\nHere are the active chatrooms:")
    # display error message if socket error
    except socket.error as e:
        print("[do_List] socket error: ", e)
        CmdWin.insert(1.0, "\nError in connecting the room server")
        return


def do_Join():
    global state, room, keepalive_class, running_thread
    # need set username before joining a chatroom
    if state == "START":
        CmdWin.insert(1.0, "\nPlease use [User] to set username first")
        return
    elif state == "NAMED":
        room = userentry.get()
        # handle empty string issue
        if room == '':
            CmdWin.insert(1.0, "\nChatroom name cannot be empty")
            return

        UDPserver_thread = threading.Thread(target=UDPserver)
        UDPserver_thread.start()
        running_thread.append(UDPserver_thread)
        print("UDPserver_thread " + str(UDPserver_thread))
        TCPserver_thread = threading.Thread(target=TCPserver)
        TCPserver_thread.start()
        running_thread.append(TCPserver_thread)
        print("TCPserver_thread " + str(TCPserver_thread))
        keepalive_class.start()
        # Update the state if first join
        state = "JOINED"

    # User is not allowed to "JOIN" again after joining a chatroom
    else:
        CmdWin.insert(1.0, "\nAlready in chatroom: " + room + ". Cannot JOIN again")
        return

    userentry.delete(0, END)
    join_request()

def join_request():
    global user, state, room, member_hash, running_thread, only_join
    try:
        request = "J:" + room + ":" + user + ":" + sockfd.getsockname()[0] + ":" + str(my_port) + "::\r\n"
        sockfd.send(request.encode('ascii'))
    except:
        sockfd.close()
        return

    rmsg = sockfd.recv(1024).decode('ascii').split(":")

    if rmsg[0] == "F":
        CmdWin.insert(1.0, "\nThere is a JOIN error from room server")
        return

    # check MSID
    if rmsg[1] == member_hash:
        return

    # update MSID
    member_hash = rmsg[1]
    # clear the user list first before getting the new list
    userlist.clear()
    # get and show the list of group members
    index = 2
    lock.acquire()
    while rmsg[index] != "":
        userlist[rmsg[index]] = (rmsg[index + 1], rmsg[index + 2])
        hashID = sdbm_hash(rmsg[index] + rmsg[index + 1] + rmsg[index + 2])
        if hashID not in mlist:
            # mlist [[hash]:(user,msgID)]
            mlist[hashID] = (rmsg[index], 0)
        index = index + 3


    hash_ulist = [sdbm_hash(user + tup[0] + tup[1]) for user, tup in userlist.items()]
    for hash in list(mlist):
        if hash not in hash_ulist:
            mlist.pop(hash)
    lock.release()
    output = ""
    for u in userlist:
        ip, port = userlist[u]
        output += "\n" + u + "\t" + ip + "\t" + port

    CmdWin.insert(1.0, output)
    CmdWin.insert(1.0, "\nHere are the users in the chatroom:")


    # start also choose P2P peer
    if only_join:
        return
    choose_peer_thread = threading.Thread(target=choose_peer)
    choose_peer_thread.start()
    running_thread.append(choose_peer_thread)
    print("choose_peer_thread "+str(choose_peer_thread))


def p2p_handshaking(peer):
    global state
    myip, myport = userlist[user]
    request = "P:" + room + ":" + user + ":" + myip + ":" + str(myport) + ":" + str(msgID) + "::\r\n"
    peer.send(request.encode("ascii"))
    # print("handshake.send:", str(peer))
    response = peer.recv(1024).decode('ascii')
    if response[0] == 'S':
        state = "CONNECTED"
        return True

    return False


def choose_peer():
    global forward_link, state, forward_socket
    gList = []
    for u in userlist:
        ip, port = userlist[u]
        gList.append((u, sdbm_hash(u + ip + port)))
    # sort by hash
    sorted(gList, key=lambda x: x[1])

    if forward_link in gList:
        return

    forward_link = ()
    forward_socket.close()

    ip, port = userlist[user]
    myHashID = sdbm_hash(user + ip + port)
    X = gList.index((user, myHashID))
    start = (X + 1) % len(gList)

    while gList[start][1] != myHashID:

        if gList[start] in backward_link:
            start = (start + 1) % len(gList)
            continue
        else:
            peer_sockfd = socket.socket()
            ip, port = userlist[gList[start][0]]
            try:
                peer_sockfd.connect((ip, int(port)))
                if p2p_handshaking(peer_sockfd):
                    # declare successfully FORWARD LINKED
                    forward_link = (gList[start])
                    print("forward" + str(forward_link))
                    # add this connection to H’s socket list??
                    forward_socket = peer_sockfd
                    # start a new connection thread to receive response from forward socket
                    TCP_server_listen_thread = threading.Thread(target=TCP_server_listen, args=(forward_socket,))
                    TCP_server_listen_thread.start()
                    running_thread.append(TCP_server_listen_thread)
                    print("TCP_server_listen_thread "+str(TCP_server_listen_thread))
                    # start_new_thread(tcp_server_listen, (forward_socket,))
                    # update gList to indicate this link??
                    return
                else:
                    start = (start + 1) % len(gList)
                    continue
            except socket.error() as e:
                start = (start + 1) % len(gList)
                continue
    if not forward_link and state != "TERMINATED":
        time.sleep(2.0)
        join_request()
    else:
        state = "CONNECTED"


# use for p2p connections
def TCPserver():
    global tcp_server_socket, backward_link, forward_socket
    tcp_server_socket.bind(("", my_port))
    CmdWin.insert(1.0, "\nMy address information " + str(udp_server_socket.getsockname()))
    tcp_server_socket.listen(5)
    while tcp_server_socket:
        # print("forever tcp listen")
        try:
            conn, addr = tcp_server_socket.accept()

            TCP_peer_listen_thread = threading.Thread(target=TCP_peer_listen, args=(conn,))
            TCP_peer_listen_thread.start()
            running_thread.append(TCP_peer_listen_thread)
            print("TCP_peer_listen_thread "+str(TCP_peer_listen_thread))
            # start_new_thread(TCP_peer_listen, (conn,))
        except:
            tcp_server_socket.close()
            break

def TCP_peer_listen(conn):
    global backward_link, mlist, state, forward_link, only_join
    while conn:
        # receive tcp connection
        try:
            data = conn.recv(1024)
        except:
            break
        rmsg = data.decode('ascii')
        _rmsg = rmsg.split(":")
        if _rmsg[0] == "P":
            peer_hash = sdbm_hash(_rmsg[2] + _rmsg[3] + _rmsg[4])

            if peer_hash not in mlist:
                only_join = True
                join_request()
                only_join = False
            if peer_hash not in mlist:
                conn.close()
                return
            backward_link.add((_rmsg[2], peer_hash))
            backward_socket[peer_hash] = conn
            print("backward" + str(backward_link))
            CmdWin.insert(1.0, "\n~~~~[" + _rmsg[2] + "]connected me~~~~")
            # send back ACK
            response = "S:" + str(msgID) + "::\r\n"
            conn.send(response.encode("ascii"))
            state = "CONNECTED"
            # print("response " + str(conn))
        elif _rmsg[0] == "T":
            if int(_rmsg[2]) not in mlist:
                only_join = True
                join_request()
                only_join = False
            if int(_rmsg[2]) not in mlist:
                print ("who are you? you are not in the room")
                continue
            if (mlist[int(_rmsg[2])][1]) < int(_rmsg[4]):
                lock.acquire()
                mlist[int(_rmsg[2])] = (_rmsg[3], int(_rmsg[4]))
                lock.release()
                index = len(_rmsg) - 2
                MsgWin.insert(1.0, "\n[" + _rmsg[3] + "] " + ':'.join(_rmsg[6:index]))
                if forward_link:
                    try:
                        forward_socket.send(data)
                    except:
                        # ensure the broken forward socket is closed
                        forward_socket.close()
                        # remove forward link if forward link broken
                        forward_link = ()
                        # rejoin to see can it find a new forward link
                        join_request_thread = threading.Thread(target=join_request)
                        join_request_thread.start()
                        running_thread.append(join_request_thread)
                        print("join_request_thread [broke forward]" + str(join_request_thread))
                        if forward_link:
                            forward_socket.send(data)

                if len(backward_link) != 0:
                    # print("check backward " + str(backward_socket))
                    broken_backward_list = []
                    for h, s in backward_socket.items():
                        try:
                            s.send(data)
                        except:
                            # ensure the socket closed if it broke
                            s.close()
                            broken_backward_list.append(h)
                    if len(broken_backward_list) == len(backward_link) and not forward_link:
                        # rejoin once to avoid it is the head of the chain and its backward dead
                        join_request_thread = threading.Thread(target=join_request)
                        join_request_thread.start()
                        running_thread.append(join_request_thread)
                        if forward_link:
                            forward_socket.send(data)
                    # remove backward link if it broke
                    if len(broken_backward_list) > 0:
                        for broken_backward_link in broken_backward_list:
                            for u, h in list(backward_link):
                                if h == broken_backward_link:
                                    backward_link.remove((u, h))
        else:
            break


def TCP_server_listen(conn):
    global forward_link
    while conn:
        try:
            data = conn.recv(1024)
            rmsg = data.decode('ascii')
            _rmsg = rmsg.split(":")
            if _rmsg[0] == "T":
                if (mlist[int(_rmsg[2])][1]) < int(_rmsg[4]):
                    lock.acquire()
                    mlist[int(_rmsg[2])] = (_rmsg[3], int(_rmsg[4]))
                    lock.release()
                    index = len(_rmsg) - 2
                    MsgWin.insert(1.0, "\n[" + _rmsg[3] + "] " + ':'.join(_rmsg[6:index]))
                    if forward_link:
                        try:
                            forward_socket.send(data)
                        except:
                            # ensure the broken forward socket is closed
                            forward_socket.close()
                            # remove forward link if forward link broken
                            forward_link = ()
                            # rejoin to see can it find a new forward link
                            join_request_thread = threading.Thread(target=join_request)
                            join_request_thread.start()
                            running_thread.append(join_request_thread)
                            print("join_request_thread [broke forward]" + str(join_request_thread))
                            if forward_link:
                                forward_socket.send(data)

                    if len(backward_link) != 0:
                        # print("check backward " + str(backward_socket))
                        broken_backward_list = []
                        for h, s in backward_socket.items():
                            try:
                                s.send(data)
                            except:
                                # ensure the socket closed if it broke
                                s.close()
                                broken_backward_list.append(h)
                        if len(broken_backward_list) == len(backward_link) and not forward_link:
                            # rejoin once to avoid it is the head of the chain and its backward dead
                            join_request_thread = threading.Thread(target=join_request)
                            join_request_thread.start()
                            running_thread.append(join_request_thread)
                            if forward_link:
                                forward_socket.send(data)
                        # remove backward link if it broke
                        if len(broken_backward_list) > 0:
                            for broken_backward_link in broken_backward_list:
                                for u, h in list(backward_link):
                                    if h == broken_backward_link:
                                        backward_link.remove((u, h))
        except:
            break


def do_Send():
    global msgID, user, forward_socket, sockfd, backward_socket, forward_link, backward_link
    my_message = userentry.get()
    if not my_message:
        CmdWin.insert(1.0, "\nERROR: Message is empty.")
        return

    if state == "CONNECTED" or len(userlist) == 1:
        my_hash = str(sdbm_hash(user + str(userlist[user][0]) + str(userlist[user][1])))
        text = "T:" + room + ":" + my_hash + ":" + user + ":" + str(msgID + 1) + ":" + str(
            len(my_message)) + ":" + my_message + "::\r\n"

        if forward_link:
            try:
                forward_socket.send(text.encode("ascii"))
            except:
                #ensure the broken forward socket is closed
                forward_socket.close()
                #remove forward link if forward link broken
                forward_link = ()
                #rejoin to see can it find a new forward link
                join_request_thread = threading.Thread(target=join_request)
                join_request_thread.start()
                running_thread.append(join_request_thread)
                print("join_request_thread [broke forward]" + str(join_request_thread))
                if forward_link:
                    forward_socket.send(text.encode("ascii"))

        if len(backward_link) != 0:
            broken_backward_list = []
            for h, s in backward_socket.items():
                try:
                    s.send(text.encode("ascii"))
                except:
                    #ensure the socket closed if it broke
                    s.close()
                    broken_backward_list.append(h)
            if len(broken_backward_list) == len(backward_link) and not forward_link:
                #rejoin once to avoid it is the head of the chain and its backward dead
                join_request_thread = threading.Thread(target=join_request)
                join_request_thread.start()
                running_thread.append(join_request_thread)
                if forward_link:
                    forward_socket.send(text.encode("ascii"))
            #remove backward link if it broke
            if len(broken_backward_list) > 0:
                for broken_backward_link in broken_backward_list:
                    for u, h in list(backward_link):
                        if h == broken_backward_link:
                            backward_link.remove((u,h))

        msgID += 1
        mlist[int(my_hash)] = (user, msgID)
        MsgWin.insert(1.0, "\n[" + user + "] " + my_message)
        userentry.delete(0, END)

    elif state == "START" or state == "NAMED" or state == "JOINED":
        CmdWin.insert(1.0, "\nERROR: Not in chatroom yet")
        return

def do_Poke():
    if state == "START" or state == "NAMED":
        CmdWin.insert(1.0, "\nYou have not joined chatroom yet")
        return
    do_Poke_Client_thread = threading.Thread(target=do_Poke_Client)
    do_Poke_Client_thread.start()
    running_thread.append(do_Poke_Client_thread)


def do_Poke_Client():
    # set up UDP client
    global user, response
    udp_client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    toBePoke = userentry.get()
    userentry.delete(0, END)
    if not toBePoke:
        for u in userlist:
            CmdWin.insert(1.0, "\n" + u)
        CmdWin.insert(1.0, "\nTo whom you want to send the poke?")
        return
    if toBePoke not in userlist:
        CmdWin.insert(1.0, "\n" + toBePoke + " is not in the chatroom, please poke other")
        return
    if toBePoke == user:
        CmdWin.insert(1.0, "\nYou cannot poke yourself")
        return
    request = "K:" + room + ":" + user + "::\r\n"
    # find the address of the user who is the poke target
    ip, port = userlist[toBePoke]
    # send the poke message via UDP
    udp_client.sendto(request.encode("ascii"), (ip, int(port)))
    # block until receiving acknowledgement
    udp_client.settimeout(2.0)
    while udp_client:
        try:
            response = udp_client.recv(1024).decode('ascii')
            if response[0] == 'A':
                MsgWin.insert(1.0, "\nReceived ACK from " + toBePoke)
                break
        except socket.timeout:
            MsgWin.insert(1.0, "\nTimeout error")
            return


def do_Quit():
    global keepalive_class, running_thread, forward_socket, backward_socket, udp_server_socket, state
    state = "TERMINATED"

    if forward_socket:
        forward_socket.close()
    for h, s in backward_socket.items():
            s.close()

    udp_server_socket.close()
    tcp_server_socket.close()

    #use look to ensure the below two event not overlaping to avoid "OSError: [Errno 9] Bad file descriptor"
    lock.acquire()
    keepalive_class.exit()
    lock.release()

    lock.acquire()
    sockfd.close()
    lock.release()

    print(running_thread)
    for thread in running_thread:
        print("quiting thread: ", thread)
        thread.join()

    sys.exit(0)


#
# Set up of Basic UI
#
win = Tk()
win.title("MyP2PChat")

# Top Frame for Message display
topframe = Frame(win, relief=RAISED, borderwidth=1)
topframe.pack(fill=BOTH, expand=True)
topscroll = Scrollbar(topframe)
MsgWin = Text(topframe, height='15', padx=5, pady=5, fg="red", exportselection=0, insertofftime=0)
MsgWin.pack(side=LEFT, fill=BOTH, expand=True)
topscroll.pack(side=RIGHT, fill=Y, expand=True)
MsgWin.config(yscrollcommand=topscroll.set)
topscroll.config(command=MsgWin.yview)

# Top Middle Frame for buttons
topmidframe = Frame(win, relief=RAISED, borderwidth=1)
topmidframe.pack(fill=X, expand=True)
Butt01 = Button(topmidframe, width='6', relief=RAISED, text="User", command=do_User)
Butt01.pack(side=LEFT, padx=8, pady=8);
Butt02 = Button(topmidframe, width='6', relief=RAISED, text="List", command=do_List)
Butt02.pack(side=LEFT, padx=8, pady=8);
Butt03 = Button(topmidframe, width='6', relief=RAISED, text="Join", command=do_Join)
Butt03.pack(side=LEFT, padx=8, pady=8);
Butt04 = Button(topmidframe, width='6', relief=RAISED, text="Send", command=do_Send)
Butt04.pack(side=LEFT, padx=8, pady=8);
Butt06 = Button(topmidframe, width='6', relief=RAISED, text="Poke", command=do_Poke)
Butt06.pack(side=LEFT, padx=8, pady=8);
Butt05 = Button(topmidframe, width='6', relief=RAISED, text="Quit", command=do_Quit)
Butt05.pack(side=LEFT, padx=8, pady=8);

# Lower Middle Frame for User input
lowmidframe = Frame(win, relief=RAISED, borderwidth=1)
lowmidframe.pack(fill=X, expand=True)
userentry = Entry(lowmidframe, fg="blue")
userentry.pack(fill=X, padx=4, pady=4, expand=True)

# Bottom Frame for displaying action info
bottframe = Frame(win, relief=RAISED, borderwidth=1)
bottframe.pack(fill=BOTH, expand=True)
bottscroll = Scrollbar(bottframe)
CmdWin = Text(bottframe, height='15', padx=5, pady=5, exportselection=0, insertofftime=0)
CmdWin.pack(side=LEFT, fill=BOTH, expand=True)
bottscroll.pack(side=RIGHT, fill=Y, expand=True)
CmdWin.config(yscrollcommand=bottscroll.set)
bottscroll.config(command=CmdWin.yview)


def main(argv):
    global serv_addr, serv_port, my_port, state
    if len(sys.argv) != 4:
        print("P2PChat.py <server address> <server port no.> <my port no.>")
        sys.exit(2)
    state = "START"
    serv_addr = argv[1]
    serv_port = int(argv[2])
    my_port = int(argv[3])
    sockfd.connect((serv_addr, serv_port))
    CmdWin.insert(1.0, "\nConnected to room server " + str(sockfd.getpeername()))
    win.mainloop()


if __name__ == "__main__":
    main(sys.argv)
